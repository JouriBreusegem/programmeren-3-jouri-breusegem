CREATE DEFINER="user303"@"%" PROCEDURE "EventTopicSelectLikeName"(
	Iname NVARCHAR(120)
)
BEGIN
	SELECT eventtopic.Id, eventTopic.Name
    FROM eventtopic
    WHERE eventtopic.Name LIKE CONCAT(Iname, '%')
    ORDER BY eventtopic.Name;
END