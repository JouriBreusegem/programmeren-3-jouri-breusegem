-- Als de database niet bestaat, wordt deze aangemaakt
CREATE DATABASE IF NOT EXISTS user303;
USE user303;

SET FOREIGN_KEY_CHECKS = 0;

CREATE table EventTopic(
	Id INT NOT NULL AUTO_INCREMENT,
    Name nvarchar(120),
    PRIMARY KEY (id)
);
create table Person(
	Firstname char(50) not null,
    Lastname char(120) not null,
    Email char(255),
    Address1 char(255),
    Address2 char(255),
    PostalCode char(20),
    City char(80),
    CountryId integer,
    Phone1 char(25),
    Birthday date,
    Rating integer,
    Id integer not null,
    Primary key(Id)
);

create table Country(
	Name char(50) not null,
    Code char(2),
    Id integer not null,
    Primary key(Id)
);

create table Role(
	Name char(50) not null,
    Id integer not null,
    primary key(Id)
);

create table User(
	Name char(50) not null,
    Salt char(255),
    HashedPassword char(255) not null,
    PersonId integer,
    RoleId integer,
    Id integer not null,
    primary key(Id)
);

create table EventCategory(
	Name char(120) not null,
    Id integer not null,
    primary key(Id)
);

create table Event(
	Name char(120) not null,
    Location char(120) not null,
    Starts datetime,
    Ends datetime,
    Image char(255) not null,
    Description varchar(1024) not null,
    OrganiserName char(120) not null,
    OrganiserDescription char(120) not null,
    EventCategoryId integer,
    EventTopicId integer,
    Id integer not null,
    primary key(Id)
);

alter table Person
add foreign key(CountryId) references Country(Id);
alter table User
add foreign key(PersonId) references Person(Id);
alter table User
add foreign key(RoleId) references Role(Id);
alter table Event
add foreign key(EventCategoryId) references EventCategory(Id);
alter table Event
add foreign key(EventTopicId) references EventTopic(Id);