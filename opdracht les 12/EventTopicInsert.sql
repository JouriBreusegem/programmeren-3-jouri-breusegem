CREATE PROCEDURE EventTopicInsert
(
	IN IeventName NVARCHAR(120),
    OUT OeventId INT
)
BEGIN
	IF NOT EXISTS (SELECT * from eventtopic WHERE Name = IeventName )
    THEN
        INSERT INTO eventtopic
        (
            eventtopic.Name
        )
        VALUES (
            IeventName
        );
        -- geeft de laatste toegevoegde rij terug
        SELECT LAST_INSERT_ID() INTO OeventId;
    else
        set OeventId = -100; -- Naam bestaat al
    END IF;
END