CREATE PROCEDURE EventTopicUpdate
(
	Iname NVARCHAR(120),
    Iid INT
)
BEGIN
UPDATE eventtopic
	SET Name = Iname
    WHERE eventtopic.Id = Iid;
END