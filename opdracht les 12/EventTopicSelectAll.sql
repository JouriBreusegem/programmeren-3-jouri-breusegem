CREATE DEFINER=`root`@`localhost` PROCEDURE `EventTopicSelectAll`()
BEGIN
	SELECT eventtopic.Name, eventtopic.Id
    FROM eventtopic
    ORDER BY eventtopic.Name;
END