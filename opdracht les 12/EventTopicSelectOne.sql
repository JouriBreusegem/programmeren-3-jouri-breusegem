CREATE DEFINER=`root`@`localhost` PROCEDURE `EventTopicSelectOne`(
	Iid INT
)
BEGIN
	SELECT eventtopic.Name, eventtopic.Id
    FROM eventtopic
		WHERE eventtopic.Id = Iid;
END