CREATE DEFINER=`root`@`localhost` PROCEDURE `EventTopicSelectLikeXName`(
	Iname NVARCHAR(120)
)
BEGIN
	SELECT eventtopic.Name, eventtopic.Id
    FROM eventtopic
    WHERE eventtopic.Name LIKE CONCAT('%', Iname, '%')
    ORDER BY eventtopic.Name;
END