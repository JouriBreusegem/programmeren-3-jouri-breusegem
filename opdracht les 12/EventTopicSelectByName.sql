CREATE DEFINER=`root`@`localhost` PROCEDURE `EventTopicSelectByName`(
	Iname NVARCHAR(120)
)
BEGIN
	SELECT eventtopic.Name, eventtopic.id
    FROM eventtopic
    WHERE eventtopic.Name = Iname
    ORDER BY eventtopic.Name;
END