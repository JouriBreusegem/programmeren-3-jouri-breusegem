﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FricFrac.Models.FricFrac
{
    public partial class Country
    {
        public Country()
        {
        }

        [Column(TypeName = "int(11)")]
        [FromForm(Name = "Country-Id")]
        public int Id { get; set; }
        [Required]
        [StringLength(2)]
        [FromForm(Name = "Country-Code")]
        public string Code { get; set; }
        [StringLength(256)]
        [FromForm(Name = "Country-Desc")]
        public string Desc { get; set; }
        [Required]
        [StringLength(50)]
        [FromForm(Name = "Country-Name")]
        public string Name { get; set; }
    }
}
