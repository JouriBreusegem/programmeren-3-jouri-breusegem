﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FricFrac.Models.FricFrac
{
    public partial class Event
    {
        [Column(TypeName = "int(11)")]
        [FromForm(Name = "Event-Id")]
        public int Id { get; set; }
        [Required]
        [StringLength(1024)]
        [FromForm(Name = "Event-Description")]
        public string Description { get; set; }
        [Column(TypeName = "datetime")]
        [FromForm(Name = "Event-Ends")]
        public DateTime? Ends { get; set; }
        [Column(TypeName = "int(11)")]
        [FromForm(Name = "Event-CategoryId")]
        public int? EventCategoryId { get; set; }
        [Column(TypeName = "int(11)")]
        [FromForm(Name = "Event-TopicId")]
        public int? EventTopicId { get; set; }
        [Required]
        [StringLength(255)]
        [FromForm(Name = "Event-Image")]
        public string Image { get; set; }
        [Required]
        [StringLength(120)]
        [FromForm(Name = "Event-Location")]
        public string Location { get; set; }
        [Required]
        [StringLength(120)]
        [FromForm(Name = "Event-Name")]
        public string Name { get; set; }
        [Required]
        [StringLength(120)]
        [FromForm(Name = "Event-OrganiserDescripton")]
        public string OrganiserDescription { get; set; }
        [Required]
        [StringLength(120)]
        [FromForm(Name = "Event-OrganiserName")]
        public string OrganiserName { get; set; }
        [Column(TypeName = "datetime")]
        [FromForm(Name = "Event-Starts")]
        public DateTime? Starts { get; set; }

        [ForeignKey("EventCategoryId")]
        public EventCategory EventCategory { get; set; }
        [ForeignKey("EventTopicId")]
        public EventTopic EventTopic { get; set; }
    }
}
