﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using FricFrac.Models.FricFrac;
using Microsoft.EntityFrameworkCore;

namespace FricFrac.Controllers
{
    public class PersonController : Controller
    {
        private readonly User303Context dbContext;

        public PersonController(User303Context context)
        {
            this.dbContext = context;
        }

        public IActionResult Index()
        {
            ViewBag.Title = "Fric-frac Person index";
            return View(dbContext.Person.ToList());
        }

        public IActionResult InsertingOne()
        {
            ViewBag.Title = "Fric-frac Person inserting one";
            return View(dbContext.Person.ToList());
        }

        [HttpGet]
        public IActionResult UpdatingOne(int? id)
        {
            ViewBag.Title = "Fric-frac Person updating one";
            if (id == null)
                return NotFound();

            var Person = dbContext.Person.SingleOrDefault(m => m.Id == id);
            if (Person == null)
                return NotFound();
            Person.Country = dbContext.Country.SingleOrDefault(m => m.Id == Person.Id);
            return View(Person);
        }

        public IActionResult Cancel()
        {
            return RedirectToAction("Index");
        }

        [HttpPost]
        public IActionResult InsertOne(Models.FricFrac.Person Person)
        {
            ViewBag.Message = "Insert een land in de database";
            dbContext.Person.Add(Person);
            dbContext.SaveChanges();
            return View("Index", dbContext.Person);
        }

        [HttpGet]
        public IActionResult ReadingOne(int? id)
        {
            ViewBag.Message = "Insert een land in de database";
            if (id == null)
                return NotFound();

            var Person = dbContext.Person.SingleOrDefault(m => m.Id == id);
            if (Person == null)
                return NotFound();

            return View(Person);
        }

        [HttpPost]
        public IActionResult UpdateOne(Models.FricFrac.Person Person)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    dbContext.Update(Person);
                    dbContext.SaveChanges();
                    return View("ReadingOne", Person);
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!dbContext.Person.Any(e => e.Id == Person.Id))
                        return NotFound();
                    else
                        throw;
                }
            }
            return View("Index", Person);
        }

        public IActionResult DeleteOne(int? id)
        {
            if (id == null)
                return NotFound();

            var Person = dbContext.Person.SingleOrDefault(m => m.Id == id);

            if (Person == null)
                return NotFound();

            dbContext.Person.Remove(Person);
            dbContext.SaveChanges();
            return RedirectToAction("Index");
        }
    }
}
