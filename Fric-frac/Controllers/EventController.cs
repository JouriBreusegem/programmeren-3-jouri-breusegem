﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FricFrac.Models.FricFrac;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace FricFrac.Controllers
{
    public class EventController : Controller
    {
        private readonly User303Context dbContext;

        public EventController(User303Context context)
        {
            this.dbContext = context;
        }

        public IActionResult Index()
        {
            ViewBag.Title = "Fric-frac Event index";
            return View(dbContext.Event.ToList());
        }

        public IActionResult InsertingOne()
        {
            ViewBag.Title = "Fric-frac Event inserting one";
            return View(dbContext.Event.ToList());
        }

        [HttpGet]
        public IActionResult UpdatingOne(int? id)
        {
            ViewBag.Title = "Fric-frac Event updating one";
            if (id == null)
                return NotFound();

            var Event = dbContext.Event.SingleOrDefault(m => m.Id == id);
            if (Event == null)
                return NotFound();

            Event.EventTopic = dbContext.EventTopic.SingleOrDefault(m => m.Id == Event.EventTopicId);
            Event.EventCategory = dbContext.EventCategory.SingleOrDefault(m => m.Id == Event.EventCategoryId);

            return View(Event);
        }

        public IActionResult Cancel()
        {
            return RedirectToAction("Index");
        }

        [HttpPost]
        public IActionResult InsertOne(Models.FricFrac.Event Event)
        {
            ViewBag.Message = "Insert een land in de database";
            dbContext.Event.Add(Event);
            dbContext.SaveChanges();
            return View("Index", dbContext.Event);
        }

        [HttpGet]
        public IActionResult ReadingOne(int? id)
        {
            ViewBag.Message = "Insert een land in de database";
            if (id == null)
                return NotFound();

            var Event = dbContext.Event.SingleOrDefault(m => m.Id == id);
            if (Event == null)
                return NotFound();

            return View(Event);
        }

        [HttpPost]
        public IActionResult UpdateOne(Models.FricFrac.Event Event)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    dbContext.Update(Event);
                    dbContext.SaveChanges();
                    return View("ReadingOne", Event);
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!dbContext.Event.Any(e => e.Id == Event.Id))
                        return NotFound();
                    else
                        throw;
                }
            }
            return View("Index", Event);
        }

        public IActionResult DeleteOne(int? id)
        {
            if (id == null)
                return NotFound();

            var Event = dbContext.Event.SingleOrDefault(m => m.Id == id);

            if (Event == null)
                return NotFound();

            dbContext.Event.Remove(Event);
            dbContext.SaveChanges();
            return RedirectToAction("Index");
        }
    }
}