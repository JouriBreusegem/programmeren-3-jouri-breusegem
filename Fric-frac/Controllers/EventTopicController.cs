﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FricFrac.Models.FricFrac;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace FricFrac.Controllers
{
    public class EventTopicController : Controller
    {
        private readonly User303Context dbContext;

        public EventTopicController(User303Context context)
        {
            this.dbContext = context;
        }

        public IActionResult Index()
        {
            ViewBag.Title = "Fric-frac EventTopic index";
            return View(dbContext.EventTopic.ToList());
        }

        public IActionResult InsertingOne()
        {
            ViewBag.Title = "Fric-frac EventTopic inserting one";
            return View(dbContext.EventTopic.ToList());
        }

        [HttpGet]
        public IActionResult UpdatingOne(int? id)
        {
            ViewBag.Title = "Fric-frac EventTopic updating one";
            if (id == null)
                return NotFound();

            var EventTopic = dbContext.EventTopic.SingleOrDefault(m => m.Id == id);
            if (EventTopic == null)
                return NotFound();
      
            return View(EventTopic);
        }

        public IActionResult Cancel()
        {
            return RedirectToAction("Index");
        }

        [HttpPost]
        public IActionResult InsertOne(Models.FricFrac.EventTopic EventTopic)
        {
            ViewBag.Message = "Insert een land in de database";
            dbContext.EventTopic.Add(EventTopic);
            dbContext.SaveChanges();
            return View("Index", dbContext.EventTopic);
        }

        [HttpGet]
        public IActionResult ReadingOne(int? id)
        {
            ViewBag.Message = "Insert een land in de database";
            if (id == null)
                return NotFound();

            var EventTopic = dbContext.EventTopic.SingleOrDefault(m => m.Id == id);
            if (EventTopic == null)
                return NotFound();

            return View(EventTopic);
        }

        [HttpPost]
        public IActionResult UpdateOne(Models.FricFrac.EventTopic EventTopic)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    dbContext.Update(EventTopic);
                    dbContext.SaveChanges();
                    return View("ReadingOne", EventTopic);
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!dbContext.EventTopic.Any(e => e.Id == EventTopic.Id))
                        return NotFound();
                    else
                        throw;
                }
            }
            return View("Index", EventTopic);
        }

        public IActionResult DeleteOne(int? id)
        {
            if (id == null)
                return NotFound();

            var EventTopic = dbContext.EventTopic.SingleOrDefault(m => m.Id == id);

            if (EventTopic == null)
                return NotFound();

            dbContext.EventTopic.Remove(EventTopic);
            dbContext.SaveChanges();
            return RedirectToAction("Index");
        }
    }
}