﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using FricFrac.Models.FricFrac;
using Microsoft.EntityFrameworkCore;

namespace FricFrac.Controllers
{
    public class CountryController : Controller
    {
        private readonly User303Context dbContext;

        public CountryController(User303Context context)
        {
            this.dbContext = context;
        }

        public IActionResult Index()
        {
            ViewBag.Title = "Fric-frac Country index";
            return View(dbContext.Country.ToList());
        }

        public IActionResult InsertingOne()
        {
            ViewBag.Title = "Fric-frac Country inserting one";
            return View(dbContext.Country.ToList());
        }

        [HttpGet]
        public IActionResult UpdatingOne(int? id)
        {
            ViewBag.Title = "Fric-frac Country updating one";
            if (id == null)
                return NotFound();

            var country = dbContext.Country.SingleOrDefault(m => m.Id == id);
            if (country == null)
                return NotFound();

            return View(country);
        }

        public IActionResult Cancel()
        {
            return RedirectToAction("Index");
        }

        [HttpPost]
        public IActionResult InsertOne(Models.FricFrac.Country Country)
        {
            ViewBag.Message = "Insert een land in de database";
            dbContext.Country.Add(Country);
            dbContext.SaveChanges();
            return View("Index", dbContext.Country);
        }

        [HttpGet]
        public IActionResult ReadingOne(int? id)
        {
            ViewBag.Message = "Insert een land in de database";
            if (id == null)
                return NotFound();

            var country = dbContext.Country.SingleOrDefault(m => m.Id == id);
            if (country == null)
                return NotFound();

            return View(country);
        }

        [HttpPost]
        public IActionResult UpdateOne(Models.FricFrac.Country country)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    dbContext.Update(country);
                    dbContext.SaveChanges();
                    return View("ReadingOne", country);
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!dbContext.Country.Any(e => e.Id == country.Id))
                        return NotFound();
                    else
                        throw;
                }
            }
            return View("Index", country);
        }

        public IActionResult DeleteOne(int? id)
        {
            if (id == null)
                return NotFound();

            var country = dbContext.Country.SingleOrDefault(m => m.Id == id);

            if (country == null)
                return NotFound();

            dbContext.Country.Remove(country);
            dbContext.SaveChanges();
            return RedirectToAction("Index");
        }
    }
}
