﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using FricFrac.Models.FricFrac;
using Microsoft.EntityFrameworkCore;

namespace FricFrac.Controllers
{
    public class RoleController : Controller
    {
        private readonly User303Context dbContext;

        public RoleController(User303Context context)
        {
            this.dbContext = context;
        }

        public IActionResult Index()
        {
            ViewBag.Title = "Fric-frac Role index";
            return View(dbContext.Role.ToList());
        }

        public IActionResult InsertingOne()
        {
            ViewBag.Title = "Fric-frac Role inserting one";
            return View(dbContext.Role.ToList());
        }

        [HttpGet]
        public IActionResult UpdatingOne(int? id)
        {
            ViewBag.Title = "Fric-frac Role updating one";
            if (id == null)
                return NotFound();

            var Role = dbContext.Role.SingleOrDefault(m => m.Id == id);
            if (Role == null)
                return NotFound();

            return View(Role);
        }

        public IActionResult Cancel()
        {
            return RedirectToAction("Index");
        }

        [HttpPost]
        public IActionResult InsertOne(Models.FricFrac.Role Role)
        {
            ViewBag.Message = "Insert een land in de database";
            dbContext.Role.Add(Role);
            dbContext.SaveChanges();
            return View("Index", dbContext.Role);
        }

        [HttpGet]
        public IActionResult ReadingOne(int? id)
        {
            ViewBag.Message = "Insert een land in de database";
            if (id == null)
                return NotFound();

            var Role = dbContext.Role.SingleOrDefault(m => m.Id == id);
            if (Role == null)
                return NotFound();

            return View(Role);
        }

        [HttpPost]
        public IActionResult UpdateOne(Models.FricFrac.Role Role)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    dbContext.Update(Role);
                    dbContext.SaveChanges();
                    return View("ReadingOne", Role);
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!dbContext.Role.Any(e => e.Id == Role.Id))
                        return NotFound();
                    else
                        throw;
                }
            }
            return View("Index", Role);
        }

        public IActionResult DeleteOne(int? id)
        {
            if (id == null)
                return NotFound();

            var Role = dbContext.Role.SingleOrDefault(m => m.Id == id);

            if (Role == null)
                return NotFound();

            dbContext.Role.Remove(Role);
            dbContext.SaveChanges();
            return RedirectToAction("Index");
        }
    }
}
