﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using FricFrac.Models.FricFrac;
using Microsoft.EntityFrameworkCore;

namespace FricFrac.Controllers
{
    public class UserController : Controller
    {
        private readonly User303Context dbContext;

        public UserController(User303Context context)
        {
            this.dbContext = context;
        }

        public IActionResult Index()
        {
            ViewBag.Title = "Fric-frac User index";
            return View(dbContext.Person.ToList());
        }

        public IActionResult InsertingOne()
        {
            ViewBag.Title = "Fric-frac User inserting one";
            return View();
        }

        [HttpGet]
        public IActionResult UpdatingOne(int? id)
        {
            ViewBag.Title = "Fric-frac User updating one";
            if (id == null)
                return NotFound();

            var User = dbContext.User.SingleOrDefault(m => m.Id == id);
            if (User == null)
                return NotFound();

            return View(User);
        }

        public IActionResult Cancel()
        {
            return RedirectToAction("Index");
        }

        [HttpPost]
        public IActionResult InsertOne(Models.FricFrac.User User)
        {
            ViewBag.Message = "Insert een land in de database";
            dbContext.User.Add(User);
            dbContext.SaveChanges();
            return View("Index", dbContext.User);
        }

        [HttpGet]
        public IActionResult ReadingOne(int? id)
        {
            ViewBag.Message = "Insert een land in de database";
            if (id == null)
                return NotFound();

            var User = dbContext.User.SingleOrDefault(m => m.Id == id);
            if (User == null)
                return NotFound();

            User.Person = dbContext.Person.SingleOrDefault(m => m.Id == User.PersonId);
            User.Role = dbContext.Role.SingleOrDefault(m => m.Id == User.RoleId);

            return View(User);
        }

        [HttpPost]
        public IActionResult UpdateOne(Models.FricFrac.User User)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    dbContext.Update(User);
                    dbContext.SaveChanges();
                    return View("ReadingOne", User);
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!dbContext.User.Any(e => e.Id == User.Id))
                        return NotFound();
                    else
                        throw;
                }
            }
            return View("Index", User);
        }

        public IActionResult DeleteOne(int? id)
        {
            if (id == null)
                return NotFound();

            var User = dbContext.User.SingleOrDefault(m => m.Id == id);

            if (User == null)
                return NotFound();

            dbContext.User.Remove(User);
            dbContext.SaveChanges();
            return RedirectToAction("Index");
        }
    }
}