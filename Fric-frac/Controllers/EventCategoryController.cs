﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FricFrac.Models.FricFrac;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace FricFrac.Controllers
{
    public class EventCategoryController : Controller
    {
        private readonly User303Context dbContext;

        public EventCategoryController(User303Context context)
        {
            this.dbContext = context;
        }

        public IActionResult Index()
        {
            ViewBag.Title = "Fric-frac EventCategory index";
            return View(dbContext.EventCategory.ToList());
        }

        public IActionResult InsertingOne()
        {
            ViewBag.Title = "Fric-frac EventCategory inserting one";
            return View(dbContext.EventCategory.ToList());
        }

        [HttpGet]
        public IActionResult UpdatingOne(int? id)
        {
            ViewBag.Title = "Fric-frac EventCategory updating one";
            if (id == null)
                return NotFound();

            var EventCategory = dbContext.EventCategory.SingleOrDefault(m => m.Id == id);
            if (EventCategory == null)
                return NotFound();

            return View(EventCategory);
        }

        public IActionResult Cancel()
        {
            return RedirectToAction("Index");
        }

        [HttpPost]
        public IActionResult InsertOne(Models.FricFrac.EventCategory EventCategory)
        {
            ViewBag.Message = "Insert een land in de database";
            dbContext.EventCategory.Add(EventCategory);
            dbContext.SaveChanges();
            return View("Index", dbContext.EventCategory);
        }

        [HttpGet]
        public IActionResult ReadingOne(int? id)
        {
            ViewBag.Message = "Insert een land in de database";
            if (id == null)
                return NotFound();

            var EventCategory = dbContext.EventCategory.SingleOrDefault(m => m.Id == id);
            if (EventCategory == null)
                return NotFound();

            return View(EventCategory);
        }

        [HttpPost]
        public IActionResult UpdateOne(Models.FricFrac.EventCategory EventCategory)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    dbContext.Update(EventCategory);
                    dbContext.SaveChanges();
                    return View("ReadingOne", EventCategory);
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!dbContext.EventCategory.Any(e => e.Id == EventCategory.Id))
                        return NotFound();
                    else
                        throw;
                }
            }
            return View("Index", EventCategory);
        }

        public IActionResult DeleteOne(int? id)
        {
            if (id == null)
                return NotFound();

            var EventCategory = dbContext.EventCategory.SingleOrDefault(m => m.Id == id);

            if (EventCategory == null)
                return NotFound();

            dbContext.EventCategory.Remove(EventCategory);
            dbContext.SaveChanges();
            return RedirectToAction("Index");
        }
    }
}