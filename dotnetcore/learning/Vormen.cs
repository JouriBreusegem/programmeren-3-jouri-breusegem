﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Wiskunde.Meetkunde
{
    class Vormen
    {
        private ConsoleColor kleur;
        public ConsoleColor Kleur
        {
            get { return kleur; }
            set
            {
                kleur = value;
                Console.ForegroundColor = kleur;
            }
        }

        public static string Lijn(int lengte)
        {
            return new string('-', lengte);
        }

        public string Lijn(int lengte, ConsoleColor kleur)
        {
            Kleur = kleur;
            return new string('-', lengte);
        }

        public string Lijn(int lengte, char teken)
        {
            return new string(teken, lengte);
        }

        public string Lijn(int lengte, char teken, ConsoleColor kleur)
        {
            Kleur = kleur;
            return new string(teken, lengte);
        }

        public void rechthoek(int hoogte, int breedte)
        {
            for (int i = 1; i <= hoogte; i++)
                Console.WriteLine(Lijn(breedte));
        }

        public void rechthoek(int hoogte, int breedte, ConsoleColor kleur)
        {
            Kleur = kleur;
            for (int i = 1; i <= hoogte; i++)
                Console.WriteLine(Lijn(breedte));
        }

        public void rechthoek(int hoogte, int breedte, char teken)
        {
            for (int i = 1; i <= hoogte; i++)
                Console.WriteLine(Lijn(breedte, teken));
        }

        public void rechthoek(int hoogte, int breedte, ConsoleColor kleur, char teken)
        {
            Kleur = kleur;
            for (int i = 1; i <= hoogte; i++)
                Console.WriteLine(Lijn(breedte, teken));
        }

        public static void driehoek(int hoogte)
        {
            int getal = hoogte;
            for (int i = 1; i <= hoogte; i++)
            {
                Console.WriteLine(Lijn(getal - i));
                getal--;
            }
        }

        public void driehoek(int hoogte, ConsoleColor kleur)
        {
            Kleur = kleur;
            int getal = hoogte;
            for (int i = 1; i <= hoogte; i++)
            {
                Console.WriteLine(Lijn(getal - i));
                getal--;
            }
        }

        public void driehoek(int hoogte, char teken)
        {
            int getal = hoogte;
            for (int i = 1; i <= hoogte; i++)
            {
                Console.WriteLine(Lijn((getal - i), teken));
                getal--;
            }
        }

        public void driehoek(int hoogte, char teken, ConsoleColor kleur)
        {
            Kleur = kleur;
            int getal = hoogte;
            for (int i = 1; i <= hoogte; i++)
            {
                Console.WriteLine(Lijn((getal - i), teken));
                getal--;
            }
        }


    }
}
