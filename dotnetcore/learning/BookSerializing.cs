﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using Newtonsoft.Json;


namespace DotNetCore.learning
{
    class BookSerializing
    {
        public static List<Book> DeserializeFromXml()
        {
            Book[] boeken;
            XmlSerializer serializer = new XmlSerializer(typeof(Book[]), new XmlRootAttribute("Books"));
            using (FileStream file = new FileStream(@"data/Book.xml", FileMode.Open))
                boeken = (Book[])serializer.Deserialize(file);

            //foreach (Book boek in boeken)
            //    Console.WriteLine(boek.ToString());

            return new List<Book>(boeken);
        }

        public static string SerializeListToCsv(List<Book> boeken, string seperator)
        {
            string message;
            XmlWriterSettings settings = new XmlWriterSettings();
            settings.NewLineHandling = NewLineHandling.None;
            settings.Indent = false;
            try
            {
                TextWriter writer = new StreamWriter(@"data/Book.csv");
                for (int i = 0; i < boeken.Count; i++)
                {
                    string text = String.Format("{0}{8}{1}{8}{2}{8}{3}{8}{4}{8}{5}{8}{6}{8}{7}",
                        boeken[i]?.Title,
                        boeken[i]?.Publisher,
                        boeken[i]?.Year,
                        boeken[i]?.Comments,
                        boeken[i]?.City,
                        boeken[i]?.Author,
                        boeken[i]?.Translator,
                        boeken[i]?.Edition,
                        seperator);
                    text = text.Trim();
                    text = text.Replace("\r", "").Replace("\n", "").Replace("\t", "");
                    writer.WriteLine(text);
                }
                message = $"Het bestand met de naam data/Book.csv is gemaakt!";
                writer.Close();
            }
            catch (Exception e)
            {
                message = $"Kan het bestand met de naam data/Book.csv niet maken.\nFoutmelding {e.Message}.";
            }

            return message;
        }

        public static List<Book> DeserializeCsvToList()
        {
            List<Book> lijst = new List<Book>();

            try
            {
                string text = File.ReadAllText(@"data/Book.csv");
                string[] boeken = text.Split("\n");

                for (int i = 0; i < boeken.Length-1; i++)
                {
                    Book book = new Book();
                    string[] values = boeken[i].Split("|");
                    book.Title = values[0];
                    book.Publisher = values[1];
                    book.Year = values[2];
                    book.Comments = values[3];
                    book.City = values[4];
                    book.Author = values[5];
                    book.Translator = values[6];
                    book.Edition = values[7];
                    lijst.Add(book);
                }

                Console.WriteLine("Csv is omgezet naar lijst!");
            }
            catch (Exception e)
            {
                Console.WriteLine($"Kan het CSV bestand niet omzetten naar een lijst. \n {e.Message}.");
            }

            return lijst;
        }

        public static string SerializeListToJson(List<Book> boeken)
        {
            string message = "";

            try
            {
                TextWriter writer = new StreamWriter(@"data/Book.json");
                writer.WriteLine(JsonConvert.SerializeObject(boeken));
                
                writer.Close();
                message = "De lijst is omgezet naar Json in data/Book.json";
            }
            catch (Exception e)
            {
                message = $"Kan de lijst niet omzetten naar Json. \nFoutmelding {e.Message}";
            }

            return message;
        }

        public static List<Book> DeserializeJsonToList()
        {
            List<Book> boeken = new List<Book>();

            try
            {
                string text = File.ReadAllText(@"data/Book.json");



                List<Book> results = JsonConvert.DeserializeObject<List<Book>>(text);
                for (int i = 0; i < results.Count; i++)
                {
                    Book boek = new Book();
                    boek.Title = results[i].Title;
                    boek.Publisher = results[i].Publisher;
                    boek.Year = results[i].Year;
                    boek.Comments = results[i].Comments;
                    boek.City = results[i].City;
                    boek.Author = results[i].Author;
                    boek.Translator = results[i].Translator;
                    boek.Edition = results[i].Edition;
                    boeken.Add(boek);
                }
                Console.WriteLine("Json is omgezet tot lijst.");
            }
            catch (Exception e)
            {
                Console.WriteLine($"Kon het bestand Book.json  niet omzetten naar lijst. \n Foutmelding: {e.Message}.");
            }

            return boeken;
        }

        public static void ShowBooks()
        {
            List<Book> boeken = new List<Book>();

            boeken = DeserializeFromXml();
            foreach(Book boek in boeken)
                Console.WriteLine(string.Format("{0}\t(1}\t{2}\t{3}\t{4}\t{5}\t{6}\t{7}\t", boek.Author, boek.City, boek.Comments, boek.Edition, boek.Publisher, boek.Title, boek.Translator, boek.Year));
        }
    }
}
