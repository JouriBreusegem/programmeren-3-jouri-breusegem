﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;

namespace DotNetCore.learning
{   
    [XmlType("Book")]
    public class Book
    {
        private string edition;
        public string Edition
        {
            get { return edition; }
            set { edition = value; }
        }

        private string author;
        public string Author
        {
            get { return author; }
            set { author = value; }
        }
        private string city;
        public string City
        {
            get { return city; }
            set { city = value; }
        }
        private string comments;
        public string Comments
        {
            get { return comments; }
            set { comments = value; }
        }
        private string publisher;
        public string Publisher
        {
            get { return publisher; }
            set { publisher = value; }
        }
        private string title;
        public string Title
        {
            get { return title; }
            set { title = value; }
        }
        private string translator;
        public string Translator
        {
            get { return translator; }
            set { translator = value; }
        }
        private string year;
        public string Year
        {
            get { return year; }
            set { year = value; }
        }

        public override string ToString()
        {
            return string.Format("Title: {0}, Publisher: {1}, Author {2}, Year: {3}, Edition: {4}, Translator: {5}, City: {6}, Comments: {7}",
                Title, Publisher, Author, Year, Edition, translator, City, Comments); 
        }
    }
}
