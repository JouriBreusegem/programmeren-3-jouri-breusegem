﻿using DotNetCore.learning;
using System;

namespace learning
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine(BookSerializing.SerializeListToCsv(BookSerializing.DeserializeFromXml(), "|"));
            BookSerializing.DeserializeCsvToList();
            Console.WriteLine(BookSerializing.SerializeListToJson(BookSerializing.DeserializeFromXml()));
            BookSerializing.DeserializeJsonToList();
            BookSerializing.ShowBooks();
            Console.ReadKey();
        }
    }
}
