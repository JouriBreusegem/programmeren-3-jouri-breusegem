﻿using System;
using System.Collections.Generic;
using System.Text;

namespace adodotnet.BLL
{
    public class EventTopic
    {
        protected string name;
        protected int id;

        public string Name
        {
            get { return this.name; }
            set { this.name = value; }
        }

        public int Id
        {
            get { return this.id; }
            set { this.id = value; }
        }
    }
}
