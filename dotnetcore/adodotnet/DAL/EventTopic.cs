﻿using System;
using System.Collections.Generic;
using System.Text;
using MySql.Data.MySqlClient;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Configuration.Json;
using System.Data;
using System.IO;
using adodotnet.BLL;

namespace adodotnet.DAL
{
    class EventTopic : IDal<BLL.EventTopic>
    {
        private string connectionString;

        private string message;
        public string Message
        {
            get { return message; }
            set { message = value; }
        }

        private int rowCount;
        public int RowCount
        {
            get { return rowCount; }
            set { rowCount = value; }
        }

        public EventTopic()
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json");
            IConfiguration configuration = builder.Build();
            this.connectionString = string.Format("server={0};user id={1};password={2};port={3};database={4};SslMode={5};",
                configuration["connection:server"],
                configuration["connection:userid"],
                configuration["connection:password"],
                configuration["connection:port"],
                configuration["connection:database"],
                configuration["connection:SslMode"]);
        }

        public int EventTopicCreate(string name)
        {
            MySqlConnection connection = new MySqlConnection(this.connectionString);
            MySqlCommand command = new MySqlCommand("EventTopicInsert", connection);
            command.CommandType = CommandType.StoredProcedure;
            MySqlParameter pName = new MySqlParameter();
            pName.ParameterName = "IeventName";
            pName.DbType = DbType.String;
            pName.Value = name;
            command.Parameters.Add(pName);
            MySqlParameter pId = new MySqlParameter();
            pId.ParameterName = "OeventId";
            pId.DbType = DbType.Int32;
            pId.Direction = ParameterDirection.Output;
            command.Parameters.Add(pId);

            Message = "Niets te melden";
            int result = 0;
            using (connection)
            {
                try
                {
                    connection.Open();
                    result = command.ExecuteNonQuery();

                    if ((int)pId.Value == -100)
                    {
                        Message = $"De categorie met de naam {name} bestaat al!";
                        result = -100;
                    }
                    else if (result <= 0)
                    {
                        Message = $"De categorie met de naam {name} kon niet worden geïnserted!";
                        this.message = "EventCategory is niet geïnserted.";
                    }
                    else
                    {
                        Message = $"De categorie met de naam {name} is geïnserted!";
                        result = (int)pId.Value;
                    }
                }
                catch (MySqlException e)
                {
                    this.message = e.Message;
                }
            }
            return result;
        }

        public List<BLL.EventTopic> EventTopicReadAll()
        {
            List<BLL.EventTopic> list = new List<BLL.EventTopic>();
            MySqlConnection connection = new MySqlConnection(this.connectionString);
            MySqlCommand command = new MySqlCommand("EventTopicSelectAll", connection);
            command.CommandType = CommandType.StoredProcedure;
            Message = "Niets te melden";
            MySqlDataReader result = null;
            RowCount = 0;
            using (connection)
            {
                try
                {
                    connection.Open();
                    Message = "Connectie is open";

                    using (result = command.ExecuteReader())
                    {
                        if (result.HasRows)
                        {
                            int counter = 0;
                            while (result.Read())
                            {
                                counter++;
                                BLL.EventTopic bll = new BLL.EventTopic();
                                bll.Name = (result.IsDBNull(result.GetOrdinal("Name")) ? "" : result["Name"].ToString());
                                bll.Id = (result.IsDBNull(result.GetOrdinal("Id")) ? 0 : Int32.Parse(result["Id"].ToString()));
                                list.Add(bll);
                            }
                            this.message = "EventTopic rijen gevonden.";
                            RowCount = counter;
                        }
                        else
                            this.message = "Geen EventTopic rijen gevonden.";
                    }
                }
                catch (MySqlException e)
                {
                    this.message = e.Message;
                }
            }
            return list;
        }

        public List<BLL.EventTopic> EventTopicReadByName(string name)
        {
            List<BLL.EventTopic> list = new List<BLL.EventTopic>();

            MySqlConnection connection = new MySqlConnection(this.connectionString);
            MySqlCommand command = new MySqlCommand("EventTopicSelectByName", connection);
            command.CommandType = CommandType.StoredProcedure;

            MySqlParameter pName = new MySqlParameter();
            pName.ParameterName = "Iname";
            pName.DbType = DbType.String;

            command.Parameters.Add(pName);

            MySqlDataReader reader = null;

            using (connection)
            {
                try
                {
                    connection.Open();

                    using (reader = command.ExecuteReader())
                    {
                        if(reader.HasRows)
                        {
                            while(reader.Read())
                            {
                                BLL.EventTopic bll = new BLL.EventTopic();
                                bll.Name = (reader.IsDBNull(reader.GetOrdinal("Name")) ? "" : reader["Name"].ToString());
                                bll.Id = (reader.IsDBNull(reader.GetOrdinal("Id")) ? 0 : Int32.Parse(reader["Id"].ToString()));
                                list.Add(bll);
                                this.message = "EventTopics met deze naam gevonden";
                            }
                        }
                        else
                        {
                            this.message = "Geen EventTopics gevonden met deze naam";
                        }
                    }
                }
                catch(Exception e)
                {
                    this.message = e.Message;
                } 
            }
            return list;
        }

        public List<BLL.EventTopic> EventTopicReadLikeName(string name)
        {
            List<BLL.EventTopic> list = new List<BLL.EventTopic>();

            MySqlConnection connection = new MySqlConnection(this.connectionString);
            MySqlCommand command = new MySqlCommand("EventTopicSelectLikeName", connection);
            command.CommandType = CommandType.StoredProcedure;

            MySqlParameter pName = new MySqlParameter();
            pName.ParameterName = "Iname";
            pName.DbType = DbType.String;

            command.Parameters.Add(pName);

            MySqlDataReader reader = null;

            using (connection)
            {
                try
                {
                    connection.Open();

                    using (reader = command.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                BLL.EventTopic bll = new BLL.EventTopic();
                                bll.Name = (reader.IsDBNull(reader.GetOrdinal("Name")) ? "" : reader["Name"].ToString());
                                bll.Id = (reader.IsDBNull(reader.GetOrdinal("Id")) ? 0 : Int32.Parse(reader["Id"].ToString()));
                                list.Add(bll);
                                this.message = "EventTopics met deze naam gevonden";
                            }
                        }
                        else
                        {
                            this.message = "Geen EventTopics gevonden met deze naam";
                        }
                    }
                }
                catch (Exception e)
                {
                    this.message = e.Message;
                }
            }
            return list;
        }

        public List<BLL.EventTopic> EventTopicReadLikeXName(string name)
        {
            List<BLL.EventTopic> list = new List<BLL.EventTopic>();

            MySqlConnection connection = new MySqlConnection(this.connectionString);
            MySqlCommand command = new MySqlCommand("EventTopicSelectLikeXName", connection);
            command.CommandType = CommandType.StoredProcedure;

            MySqlParameter pName = new MySqlParameter();
            pName.ParameterName = "Iname";
            pName.DbType = DbType.String;

            command.Parameters.Add(pName);

            MySqlDataReader reader = null;

            using (connection)
            {
                try
                {
                    connection.Open();

                    using (reader = command.ExecuteReader())
                    {
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                BLL.EventTopic bll = new BLL.EventTopic();
                                bll.Name = (reader.IsDBNull(reader.GetOrdinal("Name")) ? "" : reader["Name"].ToString());
                                bll.Id = (reader.IsDBNull(reader.GetOrdinal("Id")) ? 0 : Int32.Parse(reader["Id"].ToString()));
                                list.Add(bll);
                                this.message = "EventTopics met deze naam gevonden";
                            }
                        }
                        else
                        {
                            this.message = "Geen EventTopics gevonden met deze naam";
                        }
                    }
                }
                catch (Exception e)
                {
                    this.message = e.Message;
                }
            }
            return list;
        }

        public BLL.EventTopic EventTopicReadOne(int id)
        {
            MySqlConnection connection = new MySqlConnection(this.connectionString);
            MySqlCommand command = new MySqlCommand("EventTopicSelectOne", connection);
            command.CommandType = CommandType.StoredProcedure;
            MySqlParameter pId = new MySqlParameter();
            pId.ParameterName = "Iid";
            pId.DbType = DbType.Int32;
            pId.Value = id;
            command.Parameters.Add(pId);
            Message = "Niets te melden";
            MySqlDataReader result = null;
            RowCount = 0;
            BLL.EventTopic bll = new BLL.EventTopic();
            using (connection)
            {
                try
                {
                    connection.Open();
                    this.message = "Connectie is open";
                    using (result = command.ExecuteReader())
                    {
                        if (result.HasRows)
                        {
                            result.Read();
                            Message = $"EventCategory met Id {id} is gevonden. De naam van de categorie is {result["Name"]}";
                            bll.Name = (result.IsDBNull(result.GetOrdinal("Name")) ? "" : result["Name"].ToString());
                            bll.Id = (result.IsDBNull(result.GetOrdinal("Id")) ? 0 : Int32.Parse(result["Id"].ToString()));
                            RowCount = 1;
                        }
                        else
                            Message = $"EventCategory met Id {id} is niet gevonden.";
                    }
                }
                catch (MySqlException e)
                {
                    Message = e.Message;
                }
            }
            return bll;
        }

        public int EventTopicUpdate(BLL.EventTopic bll)
        {
            MySqlConnection connection = new MySqlConnection(this.connectionString);

            MySqlCommand command = new MySqlCommand("EventTopicUpdate", connection);

            command.CommandType = CommandType.StoredProcedure;

            MySqlParameter pId = new MySqlParameter();
            pId.ParameterName = "pId";
            pId.DbType = DbType.Int32;

            pId.Value = bll.Id;
            command.Parameters.Add(pId);
            MySqlParameter pName = new MySqlParameter();
            pName.ParameterName = "pName";
            pName.DbType = DbType.String;
            pName.Value = bll.Name;
            command.Parameters.Add(pName);

            Message = "Niets te melden";

            int result = 0;
            using (connection)
            {
                try
                {
                    connection.Open();

                    result = command.ExecuteNonQuery();

                    if (result <= 0)
                    {
                        Message = $"De categorie met de naam {bll.Name} kon niet worden geüpdated!";
                    }
                    else
                    {
                        Message = $"De categorie met de naam {bll.Name} is geüpdated!";
                    }
                }
                catch (MySqlException e)
                {
                    this.message = e.Message;
                }
                RowCount = result;
            }
            return result;
        }

        public bool EventTopicUpdate()
        {
            throw new NotImplementedException();
        }
    }
}
