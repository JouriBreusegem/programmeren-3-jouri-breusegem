﻿using System;
using System.Collections.Generic;
using System.Text;

namespace adodotnet.DAL
{
    interface IDal<T>
    {
        string Message { get; }
        int RowCount { get; }

        T EventTopicReadOne(int id);
        int EventTopicCreate(string name);
        bool EventTopicUpdate();
        List<T> EventTopicReadAll();
        List<T> EventTopicReadByName(string name);
        List<T> EventTopicReadLikeName(string name);
        List<T> EventTopicReadLikeXName(string name);
    }
}
