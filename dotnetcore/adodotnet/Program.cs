﻿using System;
using System.Collections.Generic;

namespace AdoDotNet
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Leren werken met ADO.NET in .NET Core!");
            //Learning.TestMySqlConnector();
            FricFracDalTest();
        }

        private static void FricFracDalTest()
        {
            adodotnet.DAL.EventTopic dal = new adodotnet.DAL.EventTopic();
            List<adodotnet.BLL.EventTopic> list = new List<adodotnet.BLL.EventTopic>();
            
            //Eerst halen  we alle topics op uit de tabel eventtopic en printen deze uit
            Console.WriteLine("We halen alle topics op uit de tabel eventtopic");
            list = dal.EventTopicReadAll();

            //We lezen nu een bestaand item uit de tabel in
            adodotnet.BLL.EventTopic bll = new adodotnet.BLL.EventTopic();
            Console.WriteLine("We lezen nu een bestaand topic uit de tabel in");
            bll = dal.EventTopicReadOne(10);

        }
    }
}
