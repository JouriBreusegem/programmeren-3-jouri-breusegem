﻿using System;
using System.Diagnostics;
using System.Text;
using System.Threading;

namespace werken_met_gegevens
{
    class Program
    {
        static void Main(string[] args)
        {
            Stopwatch stopwatch = new Stopwatch();   

            string strString = string.Empty;
            StringBuilder sb = new StringBuilder();

            stopwatch.Start();
            for (int i = 0; i < Int16.MaxValue; i++)
                strString = strString + "b";
            stopwatch.Stop();
            Console.WriteLine("Time elapsed string concat: {0}", stopwatch.Elapsed);
            
            stopwatch.Reset();
            stopwatch.Start();
            for (int i = 0; i < Int16.MaxValue; i++)
                sb.Append("b");
            stopwatch.Stop();
            Console.WriteLine("Time elapsed stringBuilder: {0}", stopwatch.Elapsed);



        }
    }
}
