﻿using System;
using BookApp.Bll;
using BookApp.Dal;
using Microsoft.Extensions.DependencyInjection;
using System.IO;
using Microsoft.Extensions.Configuration;

namespace BookApp
{
    class Program
    {
        static void Main(string[] args)
        {
            ServiceCollection serviceCollection = new ServiceCollection();
            ConfigureServices(serviceCollection);
            ServiceProvider serviceProvider = serviceCollection.BuildServiceProvider();
            serviceProvider.GetService<App>().Run();

            Console.ReadLine();
        }

        private static void ConfigureServices(IServiceCollection serviceCollection)
        {
            var configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", false)
                .Build();
            serviceCollection.AddOptions();
            serviceCollection.Configure<AppSettings>(configuration.GetSection("Configuration"));

            serviceCollection.AddSingleton<IBook>(a => new BookXml(new Book()));
            serviceCollection.AddSingleton<IBook>(b => new BookCsv(new Book()));
            serviceCollection.AddSingleton<IBook>(c => new BookJson(new Book()));
            serviceCollection.AddTransient<App>();
        }


    }
}
