﻿using System;
using System.Collections.Generic;
using BookApp.Bll;
using System.IO;

namespace BookApp.Dal
{
    class BookCsv : IBook
    {
        public Book Book { get; set; }
        public string Message { get; set; }
        private string connectionString = @"Data/Book";
        public string ConnectionString
        {
            get { return connectionString + ".csv"; }
            set { connectionString = value; }
        }

        public BookCsv(Book book)
        {
            Book = book;
        }

        public bool Create()
        {
            try
            {
                TextWriter writer = new StreamWriter(@"Data/Book2.csv");
                for (int i = 0; i < Book.List.Count; i++)
                    writer.WriteLine("{0}{8}{1}{8}{2}{8}{3}{8}{4}{8}{5}{8}{6}{8}{7}", Book.List[i].Title, Book.List[i].Year, Book.List[i].City, Book.List[i].Publisher, Book.List[i].Author, Book.List[i].Edition, Book.List[i].Tranlator, Book.List[i].Comment, "|");
                writer.Close();
                Message = "Het bestand Data/Book2.csv is gemaakt.";
                return true;
            }
            catch(Exception e)
            {
                Message = string.Format("Het bestand Data/Book2.csv is niet gemaakt \n Foutmelding {0}", e.Message);
                return false;
            }
        }

        public bool ReadAll()
        {
            Helpers.Tekstbestand tekstbestand = new Helpers.Tekstbestand();
            tekstbestand.FileName = ConnectionString;

            if(tekstbestand.Lees())
            {
                string[] tekstbestanden = tekstbestand.Text.Split("\n");
                try
                {
                    List<Book> list = new List<Book>();
                    for (int i = 0; i < tekstbestanden.Length; i++)
                    {
                        if(tekstbestanden[i].Length > 0)
                        {
                            Book book = new Book();
                            string[] values = tekstbestanden[i].Split("|");
                            book.Title = values[0];
                            book.Year = values[1];
                            book.City = values[2];
                            book.Publisher = values[3];
                            book.Author = values[4];
                            book.Edition = values[5];
                            book.Tranlator = values[6];
                            book.Comment = values[7];
                            list.Add(book);
                        }      
                    }
                    Book.List = list;
                    Message = string.Format("Het bestand {0} is gedeserialiseerd", ConnectionString);
                    return true;
                }
                catch(Exception e)
                {
                    Message = string.Format("Het bestand {0} is niet gedeserialiseerd \n Foutmelding: {1}", ConnectionString, e.Message);
                    return false;
                }
            }
            else
            {
                Message = string.Format("Het bestand {0} is niet gedeserialiseerd \n Foutmelding: {1}", ConnectionString, tekstbestand.Melding);
                return false;
            }
        }
    }
}
