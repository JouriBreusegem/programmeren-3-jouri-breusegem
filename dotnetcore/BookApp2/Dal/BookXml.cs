﻿using System;
using System.Collections.Generic;
using BookApp.Bll;
using System.Xml.Serialization;
using System.IO;

namespace BookApp.Dal
{
    class BookXml : IBook
    {
        public Book Book { get; set; }
        public string Message { get; set; }
        private string connectionString = @"Data/Book";
        public string ConnectionString
        {
            get { return connectionString + ".xml"; }
            set { connectionString = value; }
        }

        public BookXml(Book book)
        {
            Book = book;
        }

        public bool Create()
        {
            try
            {
                XmlSerializer serializer = new XmlSerializer(typeof(Book[]), new XmlRootAttribute("Books"));
                TextWriter writer = new StreamWriter(@"Data/Book2.xml");
                Book[] books = Book.List.ToArray();
                serializer.Serialize(writer, books);
                writer.Close();
                Message = "Het bestand Data/Book2.xml is gemaakt";
                return true;
            }
            catch(Exception e)
            {
                Message = string.Format("Het bestand Data/Book2.xml is niet niet \n Foutmelding: {0}", e.Message);
                return false;
            }
        }

        public bool ReadAll()
        {
            try
            {
                XmlSerializer serializer = new XmlSerializer(typeof(Book[]), new XmlRootAttribute("Books"));
                StreamReader file = new StreamReader(ConnectionString);
                Book[] books = (Book[])serializer.Deserialize(file);
                file.Close();
                Book.List = new List<Book>(books);
                Message = string.Format("Het bestand {0} is gedeserialiseerd.", ConnectionString);
                return true;
            }
            catch (Exception e)
            {
                Message = string.Format("Het bestand {0} is niet gedeserialiseerd.\n Foutmelding: {1}.", ConnectionString, e.Message);
                return false;
            }
        }
    }
}
