﻿using System;
using BookApp.Dal;
using Microsoft.Extensions.Options;

namespace BookApp
{
    class App
    {
        private readonly IBook bookDal;
        private readonly AppSettings appSettings;
        
        public App(IBook bookDal,  IOptions<AppSettings> appSettings)
        {
            this.bookDal = bookDal;
            this.appSettings = appSettings.Value;
        }

        public void Run()
        {
            Console.WriteLine("De Book App Generic");
            bookDal.ConnectionString = appSettings.ConnectionString;
            bookDal.ReadAll();
            Console.WriteLine(bookDal.Message);
            View.BookConsole view = new View.BookConsole(bookDal.Book);
            view.List();
            bookDal.ConnectionString = $"{appSettings.ConnectionString}3";
            bookDal.Create();
            Console.WriteLine(bookDal.Message);
        }
    }
}
