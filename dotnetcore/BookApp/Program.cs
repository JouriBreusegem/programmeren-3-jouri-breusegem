﻿using System;
using BookApp.Bll;
using BookApp.Dal;
using BookApp.View;

namespace BookApp
{
    class Program
    {
        static void Main(string[] args)
        {
            TryOutCsv();
            TryOutXml();
            TryOutJson();
            Console.ReadLine();
        }

        static void TryOutCsv()
        {
            Console.WriteLine("De BookApp CSV");
            Book book = new Book();
            BookCsv bookCsv= new BookCsv(book);
            bookCsv.Book = book;
            bookCsv.ReadAll();
            Console.WriteLine(bookCsv.Message);
            BookConsole view = new BookConsole(book);
            view.List();
            bookCsv.Create();
            Console.WriteLine(bookCsv.Message);
        }

        static void TryOutXml()
        {
            Console.WriteLine("De BookApp XML");
            Book book = new Book();
            BookXml bookXml = new BookXml(book);
            bookXml.Book = book;
            bookXml.ReadAll();
            Console.WriteLine(bookXml.Message);
            BookConsole view = new BookConsole(book);
            view.List();
            bookXml.Create();
            Console.WriteLine(bookXml.Message);
        }

        static void TryOutJson()
        {
            Console.WriteLine("De BookApp Json");
            Book book = new Book();
            BookJson bookJson = new BookJson(book);
            bookJson.Book = book;
            bookJson.ReadAll();
            Console.WriteLine(bookJson.Message);
            BookConsole view = new BookConsole(book);
            view.List();
            bookJson.Create();
            Console.WriteLine(bookJson.Message);
        }
    }
}
