﻿using System;
using System.Collections.Generic;
using System.IO;

namespace BookApp.Helpers
{
    class Tekstbestand
    {
        private string text;
        public string Text
        {
            get { return text; }
            set { text = value; }
        }

        private string melding;
        public string Melding
        {
            get { return melding; }
            set { melding = value; }
        }

        private string fileName;
        public string FileName
        {
            get { return fileName; }
            set { fileName = value; }
        }

        public bool Lees()
        {
            bool result = false;

            try
            {
                using (StreamReader sr = new StreamReader(this.FileName))
                {
                    string line;
                    List<string> lines = new List<string>();

                    while ((line = sr.ReadLine()) != null)
                        lines.Add(line.Trim());

                    this.Text = string.Join("\n", lines);
                    this.Melding = String.Format("Het bestand {0} is ingelezen", this.FileName);
                    result = true;
                }
            }
            catch(Exception e)
            {
                this.Melding = String.Format("Kan het bestand {0} niet inlezen \n Foutmelding {1}", this.Melding, e.Message);
            }

            return result;
        }
    }
}
