﻿using System;
using System.Collections.Generic;
using BookApp.Bll;
using System.IO;
using Newtonsoft.Json;

namespace BookApp.Dal
{
    class BookJson : IBook
    {
        public Book Book { get; set; }
        public string Message { get; set; }
        private string connectionString = @"Data/Book";
        public string ConnectionString
        {
            get { return connectionString + ".json"; }
            set { connectionString = value; }
        }

        public BookJson(Book book)
        {
            this.Book = book;
        }

        public bool Create()
        {
            try
            {
                TextWriter writer = new StreamWriter(@"Data/Book2.json");
                string bookString = JsonConvert.SerializeObject(Book.List);
                writer.WriteLine(bookString);
                writer.Close();
                Message = "Het bestand Data/Book2.json is gemaakt";
                return true;
            }
            catch (Exception e)
            {
                Message = string.Format("Kan het bestand met de naam Data/Book2.json niet gemaakt.\n Foutmelding: {0}.", ConnectionString, e.Message);
                return false;
            }
        }

        public bool ReadAll()
        {
            try
            {
                Helpers.Tekstbestand tekstbestand = new Helpers.Tekstbestand();
                tekstbestand.FileName = ConnectionString;
                tekstbestand.Lees();
                Book.List = JsonConvert.DeserializeObject<List<Book>>(tekstbestand.Text);
                Message = string.Format("Het bestand {0} is gedeserialiseerd", ConnectionString);
                return true;
            }
            catch (Exception e)
            {
                Message = string.Format("Het bestand {0} is niet gedeserialiseerd.\n Foutmelding: {1}.", ConnectionString, e.Message);
                return false;
            }

        }
    }
}
