﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BookApp.Dal
{
    public interface IBook
    {
        //Hier worden van Book de opgehaalde waarden in opgeslagen
        Bll.Book Book { get; set; }

        //Error meldingen
        string Message { get; set; }
        string ConnectionString { get; set; }

        //Te implementeren methodes
        bool Create();
        bool ReadAll();
    }
}
